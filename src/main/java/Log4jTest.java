//import org.slf4j.Logger;

//import org.slf4j.LoggerFactory;

import org.apache.log4j.Logger;

public class Log4jTest {

//Slf4J
//final static Logger logger = LoggerFactory.getLogger(Log4jTest.class);


    //Log4J
    final static Logger logger = Logger.getLogger(Log4jTest.class);

    public static void main(String[] args) {
        logger.info("Logging with Log 4J starts");

        Log4jTest obj = new Log4jTest();

        try{
            obj.divide();
        }catch(ArithmeticException ex){
            logger.error("Sorry, something wrong!", ex);
        }


    }

    private void divide(){

        int i = 10 /0;

    }
}